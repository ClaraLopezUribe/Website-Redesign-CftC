# Code for the Carolinas Website

Code for the Carolinas is a group of civic technology volunteers in North Carolina and South Carolina in the US. We use a Codeberg Page for our website, hosted at <a href="https://codeforthecarolinas.org/">codeforthecarolinas.org</a>. 

The website is a static Codeberg Page site that uses only HTML and a minimal amount of cascading style sheet (CSS) formatting. We are committed to providing clear, beginner-friendly explanations of every feature of Codeberg, HTML, and CSS that we use for our website.  We make these choices to help new volunteers with varied experience be able to help maintain our website. Also, as a volunteer organization, we need to be resilient to maintain the website as volunteer availability changes.

More detailed instructions for how to make changes to the website can be found in [this repository's Wiki](https://codeberg.org/Code_for_the_Carolinas/pages/wiki/?action=_pages).